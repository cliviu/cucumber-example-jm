package ticketmachine;

/**
 * Possible payment types.
 */
public enum Payment {
    CASH, CARD
}
