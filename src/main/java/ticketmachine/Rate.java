package ticketmachine;


/**
 * Possible rate types.
 */
public enum Rate {
    REDUCED, NORMAL
}
