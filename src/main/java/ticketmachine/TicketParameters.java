package ticketmachine;


public class TicketParameters {

    private Payment payment;
    private Rate rate;
    private Zone zone;

    public TicketParameters(Payment payment, Rate rate,Zone zone)
    {
        this.payment = payment;
        this.rate = rate;
        this.zone = zone;
    }


}
