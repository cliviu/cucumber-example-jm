package ticketmachine;

/**
 * Possible zones.
 */
public enum Zone {
    ZONE_1, ZONE_2, ZONE_3, ZONE_4
}
