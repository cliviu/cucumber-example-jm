package ticketmachine;


public class PriceRecord {

    private String zone;

    private String tarif;

    private String bezahlungArt;

    private String preis;


    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public String getBezahlungArt() {
        return bezahlungArt;
    }

    public void setBezahlungArt(String bezahlungArt) {
        this.bezahlungArt = bezahlungArt;
    }

    public String getPreis() {
        return preis;
    }

    public void setPreis(String preis) {
        this.preis = preis;
    }

    public String toString()
    {
        return "PriceRecord: Zone = " + zone  +  " Tarif = " + tarif + " Bezahlung Art=  " + bezahlungArt + " Preis " + preis;
    }
}
