package ticketmachine;


public class TicketMachine {

    private Zone selectedZone;

    private Payment payment;

    private Rate rate;

    private double lastCalculatedTicketPrice;


    public void startUserSession()
    {
        resetCalculatedPrice();
    }

    public void endUserSession()
    {
        resetCalculatedPrice();
    }

    private void resetCalculatedPrice()
    {
        lastCalculatedTicketPrice = -1;
    }

    public void setSelectedZone(Zone selectedZone)
    {
        this.selectedZone = selectedZone;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public void calculateTicketPrice()
    {
        lastCalculatedTicketPrice = doCalculateTicketPrice();
    }

    public double getTicketPrice()
    {
        return lastCalculatedTicketPrice;
    }

    private double doCalculateTicketPrice()
    {
        if(payment == null)
        {
            throw new RuntimeException("Payment method not selected");
        }
        if(rate == Rate.REDUCED)
        {
            if(payment == Payment.CASH)
            {
                return 1.30;
            }
            else if (payment == Payment.CARD)
            {
                return 1.2;
            }

        }
        switch(selectedZone)
        {
            case ZONE_1:
                return getTicketPriceForZone1(payment);
            case ZONE_2:
                return getTicketPriceForZone2(payment);
            case ZONE_3:
                return getTicketPriceForZone3(payment);
            case ZONE_4:
                return getTicketPriceForZone4(payment);
            default:
                throw new RuntimeException("Unknown zone");
        }
    }

    private double getTicketPriceForZone1(Payment payment)
    {
        if(payment == Payment.CASH)
        {
            return 2.6;
        }
        else if (payment == Payment.CARD)
        {
            return 2.5;
        }
        return -1;
    }


    private double getTicketPriceForZone2(Payment payment)
    {
        if(payment == Payment.CASH)
        {
            return 5.2;
        }
        else if (payment == Payment.CARD)
        {
            return 5.0;
        }
        return -1;
    }


    private double getTicketPriceForZone3(Payment payment)
    {
        if(payment == Payment.CASH)
        {
            return 7.8;
        }
        else if (payment == Payment.CARD)
        {
            return 7.5;
        }
        return -1;
    }


    private double getTicketPriceForZone4(Payment payment)
    {
        if(payment == Payment.CASH)
        {
            return 10.4;
        }
        else if (payment == Payment.CARD)
        {
            return 10.0;
        }
        return -1;
    }
}
