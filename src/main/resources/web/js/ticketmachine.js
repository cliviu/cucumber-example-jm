  angular.module('ticketmachine',  ['ui.bootstrap'])
    .controller('TicketMachineController', function($scope) {
      this.zone = '1';
      this.zones = ['1','2','3','4'];
      this.payment = 'Bargeld';
      this.payments = ['Bargeld', 'GeldKarte'];
      this.tarif = 'Normal';
      this.tarifs = ['Normal', 'Reduziert'];
      this.price = 0;
            
      this.calculatePrice = function calculatePrice() {

        if(this.tarif === 'Reduziert')
            {
                if(this.payment === 'Bargeld')
                {
                    this.price = 1.30;
                }
                else if(this.payment === 'GeldKarte')
                {
                    this.price = 1.20;
                }
            return;
        }

            if(this.zone === '1' ) {
                if(this.payment === 'Bargeld')
                {
                    this.price = 2.60; 
                }    
                else if(this.payment === 'GeldKarte')
                {
                    this.price = 2.50; 
                }   
            }
            if(this.zone === '2' ) {
                if(this.payment === 'Bargeld')
                {
                    this.price = 5.2;
                }    
                else if(this.payment === 'GeldKarte')
                {
                    this.price = 5.0; 
                }
            }
            if(this.zone === '3' ) {
                if(this.payment === 'Bargeld')
                {
                    this.price = 7.8; 
                }    
                else if(this.payment === 'GeldKarte')
                {
                    this.price = 7.5; 
                }
            }
            if(this.zone === '4' ) {
                if(this.payment === 'Bargeld')
                {
                    this.price = 10.4; 
                }    
                else if(this.payment === 'GeldKarte')
                {
                    this.price = 10.0; 
                }
            }

      };

    });