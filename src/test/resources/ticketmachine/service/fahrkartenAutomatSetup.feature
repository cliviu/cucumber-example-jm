#language:de
@fahrkartenAutomatService
Funktionalität: Fahrkartenautomat initialisieren.
  Die Fahrkartenautomat Preise sollen initialisiert werden können.

  Szenario: Fahrkartenautomat init
    Angenommen Das Fahrkartenautomat ist mit Data initialisiert
      | zone | tarif    | bezahlungArt | preis |
      | 1    | Normal   | Bargeld      | 2,6 |
      | 1    | Normal   | GeldKarte    | 2,5 |
      | 2    | Normal   | Bargeld      | 5,2 |
      | 2    | Normal   | GeldKarte    | 5,0 |
      | 3    | Normal   | Bargeld      | 7,8 |
      | 3    | Normal   | GeldKarte    | 7,5 |
      | 4    | Normal   | Bargeld      | 10,4 |
      | 4    | Normal   | GeldKarte    | 10,0 |
    Dann Das Fahrkartenautomat ist Betriebbereit

