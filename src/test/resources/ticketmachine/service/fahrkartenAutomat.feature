#language:de
@fahrkartenAutomatService
Funktionalität: Ein Fahrkartenautomat muss abhängig von den User Eingaben die richtige Fahrkarte Preis ermitteln.


  Szenario: Fahrkarte Bestellung für Sondertarif
    Angenommen Zone 1 ist selektiert
    Und Reduziert Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 1,3€

  @zone1
  Szenario: Fahrkarte Bestellung Zone 1 Barzahlung
    Angenommen Zone 1 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 2,6€

  @zone1
  Szenario: Fahrkarte Bestellung Zone 1 GeldKarte Zahlung
    Angenommen Zone 1 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit GeldKarte bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 2,5€

  @zone2
  Szenario: Fahrkarte Bestellung Zone 2 Barzahlung
    Angenommen Zone 2 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 5,2€

  @zone2
  Szenario: Fahrkarte Bestellung Zone 2 GeldKarte Zahlung
    Angenommen Zone 2 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit GeldKarte bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 5,0€

  @zone3
  Szenario: Fahrkarte Bestellung Zone 3 Barzahlung
    Angenommen Zone 3 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 7,8€

  @zone3
  Szenario: Fahrkarte Bestellung Zone 3 GeldKarte Zahlung
    Angenommen Zone 3 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit GeldKarte bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 7,5€

  @zone4
  Szenario: Fahrkarte Bestellung Zone 4 Barzahlung
    Angenommen Zone 4 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 10,40€

  @zone4
  Szenario: Fahrkarte Bestellung Zone 4 GeldKarte Zahlung
    Angenommen Zone 4 ist selektiert
    Und Normal Tarif ist selektiert
    Und Es wird mit GeldKarte bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 10,0€

