#language:de
Funktionalität: Ein Fahrkartenautomat muss abhängig von den User Eingaben die richtige Fahrkarte Preis ermitteln.

  Szenariogrundriss: Fahrkartenautomat Funktionalität testen
    Angenommen Zone <zone> ist selektiert
    Und <tarif> Tarif ist selektiert
    Und Es wird mit <bezahlungArt> bezahlt
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist <preis>€

  Beispiele:
  | zone | tarif    | bezahlungArt | preis |
  | 1    | Normal   | Bargeld      | 2,6 |
  | 1    | Normal   | GeldKarte    | 2,5 |
  | 2    | Normal   | Bargeld      | 5,2 |
  | 2    | Normal   | GeldKarte    | 5,0 |
  | 3    | Normal   | Bargeld      | 7,8 |
  | 3    | Normal   | GeldKarte    | 7,5 |
  | 4    | Normal   | Bargeld      | 10,4 |
  | 4    | Normal   | GeldKarte    | 10,0 |



