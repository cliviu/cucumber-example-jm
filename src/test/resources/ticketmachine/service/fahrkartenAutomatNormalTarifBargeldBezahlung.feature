#language:de
Funktionalität: Ein Fahrkartenautomat muss abhängig von den User Eingaben die richtige Fahrkarte Preis ermitteln.

  Grundlage: Normal Tarif ist selektiert und es wird mit Bargeld bezahlt
    Angenommen Normal Tarif ist selektiert
    Und Es wird mit Bargeld bezahlt

  Szenario: Fahrkarte Bestellung Zone 1 Barzahlung
    Angenommen Zone 1 ist selektiert
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 2,6€

  Szenario: Fahrkarte Bestellung Zone 2 Barzahlung
    Angenommen Zone 2 ist selektiert
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 5,2€

  Szenario: Fahrkarte Bestellung Zone 3 Barzahlung
    Angenommen Zone 3 ist selektiert
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 7,8€

  Szenario: Fahrkarte Bestellung Zone 4 Barzahlung
    Angenommen Zone 4 ist selektiert
    Wenn Der Fahrkartenpreis berechnet wird
    Dann Der Fahrkartenpreis ist 10,40€