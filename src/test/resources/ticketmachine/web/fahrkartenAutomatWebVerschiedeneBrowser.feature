#language:de
@fahrkartenAutomatWeb
Funktionalität: Fahrkarten Bestellungen über die Web Seite testen

  Szenariogrundriss: Fahrkartenautomat funktionalität testen
    Angenommen Erste Seite ist geoeffnet in Firefox
    Und Zone <zone> ist in Seite selektiert
    Und <tarif> Tarif ist in Seite selektiert
    Und Bezahlung mit <bezahlungArt> ist selektiert
    Wenn Die Fahrkarte Preis Berechung getriggered wird
    Dann Die angezeigte Fahrkarte Preis ist <preis>€

  Beispiele:
    | zone | tarif    | bezahlungArt | preis |
    | 1    | Normal   | Bargeld      | 2.6   |
    | 1    | Normal   | GeldKarte    | 2.5   |
    | 2    | Normal   | Bargeld      | 5.2   |
    | 2    | Normal   | GeldKarte    | 5     |
    | 3    | Normal   | Bargeld      | 7.8   |
    | 3    | Normal   | GeldKarte    | 7.5   |
    | 4    | Normal   | Bargeld      | 10.4  |
    | 4    | Normal   | GeldKarte    | 10    |
