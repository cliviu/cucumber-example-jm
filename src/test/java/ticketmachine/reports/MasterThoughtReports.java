package ticketmachine.reports;

import net.masterthought.cucumber.ReportBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lcarausu on 25.10.14.
 */
public class MasterThoughtReports {

    public static void main(String[] args)  throws Exception
    {
        File reportOutputDirectory = new File("target/cucumber-html-json-reports");
        List<String> jsonReportFiles = new ArrayList<String>();
        jsonReportFiles.add("target/cucumber-json-report");

        String buildNumber = "1";
        String buildProjectName = "super_project";
        Boolean skippedFails = false;
        Boolean undefinedFails = false;
        Boolean flashCharts = true;
        Boolean runWithJenkins = false;
        ReportBuilder reportBuilder = new ReportBuilder(jsonReportFiles,reportOutputDirectory,"","95","cucumber-jvm",false,false,true,true,false,"",true);
        reportBuilder.generateReports();

    }
}
