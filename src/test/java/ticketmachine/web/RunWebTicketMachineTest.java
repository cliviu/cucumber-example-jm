package ticketmachine.web;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by lcarausu on 04.10.14.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/ticketmachine/web/fahrkartenAutomatWeb.feature"} ,
                 format = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber-json-report"})
public class RunWebTicketMachineTest {
}
