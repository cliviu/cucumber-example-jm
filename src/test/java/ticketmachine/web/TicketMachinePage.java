package ticketmachine.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;


public class TicketMachinePage {

    @FindBys({@FindBy(id="zoneSelector"),@FindBy(tagName="option")})
    List<WebElement> zoneSelector;

    @FindBys({@FindBy(id="tarifSelector"),@FindBy(tagName="option")})
    List<WebElement> tarifSelector;

    @FindBys({@FindBy(id="paymentSelector"),@FindBy(tagName="option")})
    List<WebElement> paymentSelector;

    @FindBy(id="triggerCalculate")
    WebElement priceCalculatorTrigger;

    @FindBy(id="price")
    WebElement price;

    public void selectZone(int selectedZone) {
        clickSelectedOption(String.valueOf(selectedZone), zoneSelector);
    }

    public void selectTarif(String selectedTarif) {
        clickSelectedOption(selectedTarif, tarifSelector);
    }

    public void selectPayment(String selectedPayment) {
        clickSelectedOption(selectedPayment, paymentSelector);
    }

    public void calculatePrice()
    {
        priceCalculatorTrigger.click();
    }

    public String getCalculatedTicketPrice()
    {
        return price.getText();
    }

    private void clickSelectedOption(String selectedOption, List<WebElement> elements) {
        for(WebElement element : elements)
        {
            if(element.getAttribute("value").equals(selectedOption)) {
                element.click();
                break;
            }
        }
    }
}
