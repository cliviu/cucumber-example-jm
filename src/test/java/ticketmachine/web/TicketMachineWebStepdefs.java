package ticketmachine.web;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class TicketMachineWebStepdefs {

    public static final String TARIF_NORMAL = "Normal";
    public static final String TARIF_REDUZIERT = "Reduziert";
    public static final String BEZAHLUNG_BARGELD = "Bargeld";
    public static final String BEZAHLUNG_GELD_KARTE = "GeldKarte";

    private WebDriver webDriver;

    private TicketMachinePage ticketMachinePage;

    @Angenommen("^Erste Seite ist geoeffnet$")
    public void firstPageIsSelected() throws Throwable
    {
        webDriver = new PhantomJSDriver();
        initializeWebDriver();
        webDriver.get("http://localhost:" + "8080");
    }

    @Angenommen("^Erste Seite ist geoeffnet in (.*)$")
    public void firstPageIsSelectedInBrowser(String webBrowser) throws Throwable {
        if(webBrowser.equals("Firefox"))
        {
            webDriver = new FirefoxDriver();
        }
        else if(webBrowser.equals("Chrome"))
        {
            System.setProperty("webdriver.chrome.driver","/usr/bin/google-chrome");
            webDriver = new ChromeDriver();
        }
        else
        {
            webDriver = new PhantomJSDriver();
        }
        initializeWebDriver();
        webDriver.get("http://localhost:" + "8080");
    }

    private void initializeWebDriver() {
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        ticketMachinePage = PageFactory.initElements(webDriver, TicketMachinePage.class);

    }


    @Angenommen("^Zone (.*) ist in Seite selektiert$")
    public void zone_Is_Selected(int selectedZone) throws Throwable {
        ticketMachinePage.selectZone(selectedZone);
    }

    @Angenommen("^(.*) Tarif ist in Seite selektiert$")
    public void tarif_Is_Selected(String selectedTarif) throws Throwable
    {
        ticketMachinePage.selectTarif(selectedTarif);
    }

    @Angenommen("^Bezahlung mit (.*) ist selektiert$")
    public void payment_Is_Selected(String selectedPaymentType) throws Throwable
    {
        ticketMachinePage.selectPayment(selectedPaymentType);
    }

    @Wenn("Der Fahrkartenpreis Berechung getriggered wird")
    public void ticket_price_is_calculated() throws Throwable
    {
        ticketMachinePage.calculatePrice();
    }

    @Dann("Der angezeigte Fahrkartenpreis ist (.*)€$")
    public void calculate_ticket_price(String ticketPrice) throws Throwable
    {
        assertThat(ticketPrice).isEqualTo(ticketMachinePage.getCalculatedTicketPrice());
    }

    @After
    public void embedScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                byte[] screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException wde) {
                System.err.println(wde.getMessage());
            } catch (ClassCastException cce) {
                cce.printStackTrace();
            }
        }
        webDriver.close();
    }

}
