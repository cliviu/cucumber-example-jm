package ticketmachine.service;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Und;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.java.StepDefAnnotation;
import org.picocontainer.annotations.Inject;
import ticketmachine.Payment;
import ticketmachine.Rate;
import ticketmachine.TicketMachine;
import ticketmachine.Zone;

import static org.fest.assertions.Assertions.assertThat;



public class TicketMachineGlue {

    public static final String TARIF_NORMAL = "Normal";
    public static final String TARIF_REDUZIERT = "Reduziert";
    public static final String BEZAHLUNG_BARGELD = "Bargeld";
    public static final String BEZAHLUNG_GELD_KARTE = "GeldKarte";

    private TicketMachine ticketMachine;

    public TicketMachineGlue(TicketMachine ticketMachine) {
        this.ticketMachine =  ticketMachine;
    }

    @Angenommen("^Zone (\\d+) ist selektiert$")
    public void selectZone(int selectedZone) throws Throwable
    {
        ticketMachine.setSelectedZone(Zone.values()[selectedZone - 1]);
    }

    @Und("(.*) Tarif ist selektiert$")
    public void selectTarifType(String selectedTarif) throws Throwable
    {
        if(selectedTarif.equals(TARIF_NORMAL))
        {
            ticketMachine.setRate(Rate.NORMAL);
        }
        else if(selectedTarif.equals(TARIF_REDUZIERT))
        {
            ticketMachine.setRate(Rate.REDUCED);
        }
    }

    @Und("Es wird mit (.*) bezahlt$")
    public void selectPaymentType(String selectedPaymentType) throws Throwable
    {
        if(selectedPaymentType.equals(BEZAHLUNG_BARGELD))
        {
            ticketMachine.setPayment(Payment.CASH);
        }
        else if(selectedPaymentType.equals(BEZAHLUNG_GELD_KARTE))
        {
            ticketMachine.setPayment(Payment.CARD);
        }
    }

    @Wenn("Der Fahrkartenpreis berechnet wird$")
    public void calculateTicketPrice(){
        ticketMachine.calculateTicketPrice();
    }

    @Dann("Der Fahrkartenpreis ist (.*)€$")
    public void checkCorrectTicketPrice(double ticketPrice) throws Throwable
    {
        assertThat(ticketPrice).isEqualTo(ticketMachine.getTicketPrice());
    }

    @Before(value = "@fahrkartenAutomatService")
    public void beforeScenario()
    {
        ticketMachine.startUserSession();
    }

    @After(value = "@fahrkartenAutomatService",order = 1)
    public void afterScenarioOrder1()
    {
        ticketMachine.endUserSession();
    }

    @After(value = "@fahrkartenAutomatService",order = 2)
    public void afterScenarioOrder2() {
    }
}
