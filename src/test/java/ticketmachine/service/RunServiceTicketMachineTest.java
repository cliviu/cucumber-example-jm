package ticketmachine.service;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 *
 * @author Liviu Carausu (liviu.carausu@gmail.com)
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/ticketmachine/service/fahrkartenAutomat.feature", "src/test/resources/ticketmachine/service/fahrkartenAutomatScenarioOutline.feature"},
        glue = {"ticketmachine.service"},
        format = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber-json-report"})
public class RunServiceTicketMachineTest {
}
