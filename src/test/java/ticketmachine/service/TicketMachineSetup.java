package ticketmachine.service;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import gherkin.formatter.model.DataTableRow;
import ticketmachine.PriceRecord;

import java.util.List;

/**
 * Created by lcarausu on 24.10.14.
 */
public class TicketMachineSetup {

    @Angenommen("^Das Fahrkartenautomat ist mit Data initialisiert$")
    public void setupTicketMachine(List<PriceRecord> priceRecords) throws Throwable {
        for(PriceRecord priceRecord : priceRecords)
        {
            // store the records in  a database
            System.out.println(priceRecord);
        }
    }

    @Dann("^Das Fahrkartenautomat ist Betriebbereit$")
    public void ticketMachineIsReady() throws Throwable {

    }
}
